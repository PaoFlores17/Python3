![Logo Python](py3.png)

# Python 3

## Indice

* [Buenas practicas de programacion](#buenas-practicas-de-programacion)
* [Asignacion multiple](#asignacion-multiple)
* [Tipos de datos numericos en Python](#tipos-de-datos-numericos-en-python)
* [Operadores](#operadores)
* [Cadenas de texto](#cadenas-de-texto)
* [Listas](#listas)
* [Tuplas](#tuplas)
* [Diccionarios](#diccionarios)
* [Operadores relacionales](#operadores-relacionales)
* [Operadores lógicos](#operadores-lógicos)
* [Estructuras de control](#estructuras-de-control)
* [Estructuras iterativas](#estructuras-iterativas)
* [List Comprehensions](#list-comprehensions)
* [Funciones](#funciones)
* [Argumentos](#argumentos)
* [Modulos](#modulos)
* [Paquetes](#paquetes)
* [Para la Amiau](#para-la-amiau)

## Buenas practicas de programacion

* Las variables siempre deben estar en minusculas y usar la notacion "snake case"

* Se tiene que definir el encabezado de python para declarar que version de python se esta usando y evitar problemas de unicode  `#!/usr/bin/python3`

* Cuando se crea una clase, el nombre de la clase debe empezar con mayuscula y usar la notacion "camel case"

---

## Asignacion multiple

Asignarle valores a multiples variables

~~~python
x, y, z = 1, "Hola", True
~~~

---

## Tipos de datos numericos en Python

### Entero

Valores numericos enteros

~~~python
variable_entero = 5
~~~

### Flotante

Valores numericos flotantes o reales

~~~python
variable_flotante = 5.366666
~~~

### Complejo

Valores imaginarios, estos valores se representan colocando una "j" seguida del valor

~~~python
variable_compleja = 5.3j
~~~

### Boleano

Valores binarios que solo regresan 2 tipos de salidas: "True" o "False"

~~~python
variable_boleana_verdadera = True
variable_boleana_falsa = False
~~~

---

## Operadores

### Operaciones matematicas

~~~python
suma = valor1 + valor2
resta = valor1 - valor2
division_flotante = valor1 / valor2
division_entera = valor1 // valor2
residuo = valor1 % valor2
potencia = valor1 ** valor2
~~~

---

## Cadenas de texto

### Cadenas simples

Texto que se asigna a una variables

~~~python
variable_texto = "Esto es un texto"
~~~

### Concatenado de Cadenas

Union de mas de una cadena de texto

~~~python
menjaje1 = "Primer cadena"
mensaje2 = "Segunda cadena"

concatenado1 = "Salida: " + mensaje1 + " mas " + mensaje2

concatenado2 = "Salida: ", mensaje1, " mas ", mensaje2

concatenado3 = "Salida: %s mas %s" %(mensaje1, mensaje2)

concatenado4 = "Salida: {} mas {}".format(mensaje1, mensaje2)
~~~

### Operaciones en Cadenas

*NOTA: Al realizar este tipo de operaciones en Pyrhon, se obtienen subcadenas de la cadena original.*

Para recuperar el valor en una posicion determinada de una cadena se usa un indice

~~~python
cadena = "Esto es una cadena de texto"
print(cadena[3])

Salida >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
o
~~~

Imprimir un unico segmento de la cadena

~~~python
cadena = "Esto es una cadena de texto"
print(cadena[5:18])

Salida >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
es una cadena
~~~

Es posible realizar esta accion de forma inversa

~~~python
cadena = "Esto es una cadena de texto"
print(cadena[-10:])

Salida >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
a de texto
~~~

Imprimir un segmento de cadena saltando una determinada cantidad de espacios en la subcadena

~~~python
cadena = "Esto es una cadena de texto"
print(cadena[0:18:2])

Salida >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
Et suacdn
~~~

Imprimir la cadena de texto de forma inversa

~~~python
cadena = "Esto es una cadena de texto"
print(cadena[::-1])

Salida >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
otxet ed anedac anu se otsE
~~~

### Metodos para Cadenas

#### Formatos

Asignar un alias en el metodo `.format`

~~~python
cadena_primera = "Primera"
cadena_segunda = "SEGUNDA"

cadena = "primero va {b} y depues {a}".format(a = cadena_segunda, b = cadena_primera)

print(cadena)

Salida >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
primero va Primera y depues SEGUNDA
~~~

Pasar todos los caracteres de la cadena a minusculas

~~~python
cadena_primera = "Primera"
cadena_segunda = "SEGUNDA"

cadena = "primero va {b} y depues {a}".format(a = cadena_segunda, b = cadena_primera)

cadena = cadena.lower()

print(cadena)

Salida >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
primero va primera y depues segunda
~~~

Pasar todos los caracteres de la cadena a mayusculas

~~~python
cadena_primera = "Primera"
cadena_segunda = "SEGUNDA"

cadena = "primero va {b} y depues {a}".format(a = cadena_segunda, b = cadena_primera)

cadena = cadena.upper()

print(cadena)

Salida >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
primero va primera y depues segunda
~~~

#### Busquedas

Busca la posicion en la que inicia un determinado texto

~~~python
cadena_de_texto = "Esta es una cadena de texto simple"

busqueda = cadena_de_texto.find("texto")

print(busqueda)

Salida >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
22
~~~

Regresa la cantidad de veces que aparece un caracter en la cadena

~~~python
cadena_de_texto = "Esta es una cadena de texto simple"

busqueda = cadena_de_texto.count("e")

print(busqueda)

Salida >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
5
~~~

#### Modificaciones

Remplaza un caracter por otro, en este ejemplo remplazara las "e" por "X"

~~~python
cadena_de_texto = "Esta es una cadena de texto simple"

nueva_cadena = cadena_de_texto.replace("e", "X")

print(nueva_cadena)

Salida >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
Esta Xs una cadXna dX tXxto simplX
~~~

Divide la cadena y la almacena en una lista al encontrar un caracter especificado, en este caso el caracter de division es un espacio " "

~~~python
cadena_de_texto = "Esta es una cadena de texto simple"

nueva_cadena = cadena_de_texto.split(" ")

print(nueva_cadena)

Salida >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
['Esta', 'es', 'una', 'cadena', 'de', 'texto', 'simple']
~~~

---

## Listas

Las listas a diferencia de las cadenas de texto, pueden alamacenar diferentes tipos de datos, tambien pueden poseer un tamaño variable.

~~~python
lista = ["string", 15, 3.5, False]

print(lista)

Salida >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
['string', 15, 3.5, False]
~~~

Añadir un elemento al final de la lista

~~~python
lista = ["string", 15, 3.5, False]

lista.append(6)

print(lista)

Salida >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
['string', 15, 3.5, False, 6]
~~~

Añadir un dato a la lista en una posicion determinada

~~~python
#!/usr/bin/python3

lista = ["string", 15, 3.5, False]

lista.insert(2, "añadido")

print(lista)

Salida >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
['string', 15, 'añadido', 3.5, False]
~~~

Eliminar un dato de la lista

~~~python
lista = ["string", 15, 3.5, False]

lista.remove(15)

print(lista)

Salida >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
['string', 3.5, False]
~~~

Eliminar el ultimo valor de la lista

~~~python
lista = ["string", 15, 3.5, False]

lista.pop()

print(lista)

Salida >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
['string', 15, 3.5]
~~~

Ordenar los elementos de una lista del menor al mayoyor

~~~python
lista = [1,8,3,7,2,9,5,8,32,4,8]

lista.sort()

print(lista)

Salida >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
[1, 2, 3, 4, 5, 7, 8, 8, 8, 9, 32]
~~~

Oredenar los elementos de una lista del mayor al menor

~~~python
lista = [1,8,3,7,2,9,5,8,32,4,8]

lista.sort(reverse = True)

print(lista)

Salida >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
[32, 9, 8, 8, 8, 7, 5, 4, 3, 2, 1]
~~~

Unir 2 Listas

~~~python
lista_uno = [1,3,2,5,7,4,8]
lista_dos = [11, 12, 13]

lista_uno.extend(lista_dos)

print(lista_uno)

Salida >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
[1, 3, 2, 5, 7, 4, 8, 11, 12, 13]
~~~

Añadir una lista dentro de otra lista

~~~python
lista_uno = [1,3,2,5,7,4,8]
lista_dos = [11, 12, 13]

lista_uno.append(lista_dos)

print(lista_uno)

Salida >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
[1, 3, 2, 5, 7, 4, 8, 11, 12, 13]
~~~

---

## Tuplas

Las tuplas son parecidas a las listas solo que este tipo de listas no pueden ser modificadas. Adiferencia de las listas, estas pueden ser usadas como clabes para los diccionarios.

Las tuplas ocupan menos espacio en memoria que las listas

~~~python
tupla = (1, 2, 3, 4)

print(tupla)

Salida >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
(1, 2, 3, 4)
~~~

---

## Diccionarios

Los diccionarios son listas a los cuales se les puede asignar un identificador para cada elemento de la lista, se pueden usar tuplas como identificadores para el diccionario

En los diccionarios no se puede aplicar el "slicing" (Secciones determinadas de la lista)

~~~python
diccionario = {
"Numero" : 1,
"Texto" : "Esto es un texto",
"Booleano" : True,
"Tupla" : (1, 2, 3),
"Lista" : [4, 5, 6]
}

print(diccionario)

Salida >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
{'Numero': 1, 'Booleano': True, 'Lista': [4, 5, 6], 'Texto': 'Esto es un texto', 'Tupla': (1, 2, 3)}
~~~

Modificar un valor dentro del diccionario

~~~python
diccionario = {
"Numero" : 1,
"Texto" : "Esto es un texto",
"Booleano" : True,
"Tupla" : (1, 2, 3),
"Lista" : [4, 5, 6]
}

diccionario["Tupla"] = (7, 8, 9)

print(diccionario)

Salida >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
{'Tupla': (7, 8, 9), 'Lista': [4, 5, 6], 'Numero': 1, 'Booleano': True, 'Texto': 'Esto es un texto'}
~~~

Eliminar entradas del diccionario

~~~python
diccionario = {
"Numero" : 1,
"Texto" : "Esto es un texto",
"Booleano" : True,
"Tupla" : (1, 2, 3),
"Lista" : [4, 5, 6]
}

del(diccionario["Booleano"])

print(diccionario)

Salida >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
{'Numero': 1, 'Lista': [4, 5, 6], 'Tupla': (1, 2, 3), 'Texto': 'Esto es un texto'}
~~~

En el caso de que se busque un valor que no se encuentre en el diccionario, se puede regresar un mensaje de error o
algun otro dato.
En este ejmplo, se pedira un valor "x", pero este al no existir regresara el valor de error indicado.

~~~python
diccionario = {
"Numero" : 1,
"Texto" : "Esto es un texto",
"Booleano" : True,
"Tupla" : (1, 2, 3),
"Lista" : [4, 5, 6]
}

diccionario = diccionario.get("x", "Valor inexistente en el diccionario")

print(diccionario)

Salida >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
Valor inexistente en el diccionario
~~~

Se pueden obtener unicamente las llaves o los valores del diccionario mediante los metodos **keys()** y
**values()**

~~~python
diccionario = {
"Numero" : 1,
"Texto" : "Esto es un texto",
"Booleano" : True,
"Tupla" : (1, 2, 3),
"Lista" : [4, 5, 6]
}

print(diccionario)

llaves = diccionario.keys()
valores = diccionario.values()

print(llaves)
print(valores)

Salida >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
{'Lista': [4, 5, 6], 'Numero': 1, 'Booleano': True, 'Tupla': (1, 2, 3), 'Texto': 'Esto es un texto'}
dict_keys(['Lista', 'Numero', 'Booleano', 'Tupla', 'Texto'])
dict_values([[4, 5, 6], 1, True, (1, 2, 3), 'Esto es un texto'])
~~~

si se quiere que los datos devueltos por **keys()** y **values()** regresen como listas puras,
se puede usar la función **list()** o la funcion **tuple()** si se quiere una tupla

~~~python
diccionario = {
"Numero" : 1,
"Texto" : "Esto es un texto",
"Booleano" : True,
"Tupla" : (1, 2, 3),
"Lista" : [4, 5, 6]
}

print(diccionario)

llaves = list(diccionario.keys())
valores = tuple(diccionario.values())

print(llaves)
print(valores)

Salida >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
{'Numero': 1, 'Booleano': True, 'Tupla': (1, 2, 3), 'Texto': 'Esto es un texto', 'Lista': [4, 5, 6]}
['Numero', 'Booleano', 'Tupla', 'Texto', 'Lista']
(1, (1, 2, 3), True, 'Esto es un texto', [4, 5, 6])
~~~

Añadir un diccionario a otro diccionario

~~~python
diccionario_uno = {"uno" : 1, "dos" : 2, "tres" : 3}
diccionario_dos = {"a" : 4, "b" : 5, "c" : 6}

diccionario_uno.update(diccionario_dos)

print(diccionario_uno)

Salida >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
{'tres': 3, 'dos': 2, 'uno': 1, 'a': 4, 'c': 6, 'b': 5}
~~~

---

## Operadores Relacionales

| Símbolo  | Significado        | Ejemplo       | Resultado |
| :----    | :----:             | :----:        | ----:     |
| ==       | Igual que          | 5 == 7        | Falso     |
| !=       | Distinto que       | rojo != verde | Verdadero |
| <        | Menor que          | 8 < 12        | Verdadero |
| >        | Mayor que          | 12 > 7        | Falso     |
| <=       | Menor o igual que  | 12 <= 12      | Verdadero |
| >=       | Mayor o igual que  | 4 >= 5        | Falso     |

---

## Operadores Lógicos

| Operador | Significado | Ejemplo                   | Resultado |
| :----    | :----:      | :----:                    | ----:     |
| AND      | y           | (5 and 7) < 10            | True      |
| AND      | y           | (5 and 11) < 10           | False     |
| OR       | o           | (2 or 4) in [1,2,3,4,5,6] | True      |
| OR       | o           | (2 or 8) in [1,2,3,4,5,6] | False     |
| NOT      | negación    | not(True)                 | False     |
| NOT      | negación    | not(False)                | True      |

---

## Estructuras de control

Condicionales , Estructuras de seleccion o Estructuras de flujo

Sirven para tomar una decicion y ejecutar un bloque de instrucciones

### If, Else, Elif

~~~python
numero = 5

if numero > 0 :      # Si el numero en mayor a 0
    print("Positivo")
elif numero < 0 :    # Si el numero es menor a 0
    print("Negativo")
else :               # Si es una opcion no conciderada en las anteriores
    print("Nulo")

Salida >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
Positivo
~~~

En el caso de que se quiera crear una condicional aun sin saber el codigo que va
ha ejecutar dicha condicion se puede hacer uso de la palabra reservada **pass**

~~~python
numero = 5

if numero > 0 :
    print("Positivo")
elif numero < 0 :
    print("Negativo")
elif numero == None :
  pass
else :               
    print("Nulo")

Salida >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
Positivo
~~~

*NOTA: Si ha esta estructura no se le pasara el singleton* **pass** *a la condicional,
la ejecucion regresaria un error de identacion*

---

## Estructuras iterativas

Ejecutan un bloque de codigo hasta que una condicion se cumpla, deje de cumplirse o se itere una determinada cantidad de veces

### While

El siclo se va a realizar **mientras** la condicion se cumpla

~~~python
numero = 1

while numero <= 4 :
    print("Numero: ", numero)
    numero += 1

Salida >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
Numero: 1
Numero: 2
Numero: 3
Numero: 4
~~~

### For

Siclo para recorrer listas o tuplas con ayuda de una variable de nombre **contador**

NOTA: *La idea tras esta estructura seria: Por cada `elemento` en esta `lista` imprimir elementos*

~~~python
lista = [1, 2.5, True, "cadena"]

for elemento in lista :
    print(elemento)

Salida >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
1
2.5
True
cadena
~~~

Otra implementacion del ciclo for usando el metodo `range()`, en este caso `range()` ayuda a definir un rango de valores para imprimir

~~~python
for numero in range(1, 5) :
    print(numero)

Salida >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
1
2
3
4
~~~

Ejemplo practico del ciclo **for**:

~~~python
numero = input("Introdusca un numero: ")
numero = int(numero)
contador = list(range(1, 11))

for elemento in contador :
    print(numero, "X", elemento, "=", numero*elemento)

Salida >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
Introdusca un numero: 9
(9, 'X', 1, '=', 9)
(9, 'X', 2, '=', 18)
(9, 'X', 3, '=', 27)
(9, 'X', 4, '=', 36)
(9, 'X', 5, '=', 45)
(9, 'X', 6, '=', 54)
(9, 'X', 7, '=', 63)
(9, 'X', 8, '=', 72)
(9, 'X', 9, '=', 81)
(9, 'X', 10, '=', 90)

~~~

*NOTA: Se pueden usar las palabras reservadas* **break** *y* **continue** *para
interrumpir o continuar la ejecucion de un ciclo*

---

## List Comprehensions

Es la construcción de listas de forma "natural", parecida a como lo haria un matematico

~~~python
S = [x**2 for x in range(10)]
V = [2**i for i in range(13)]
M = [x for x in S if x % 2 == 0]

print(S) # Crear una lista con una lista del 0 al 10 como base de potencia
print(V) # Crear una lista con una potencia que va del 0 al 13
print(M) # Crear una lista con los números pares de la primer lista

Salida >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
[0, 1, 4, 9, 16, 25, 36, 49, 64, 81]
[1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096]
[0, 4, 16, 36, 64]
~~~

---

## Funciones

Son bloques de codigo a los que se les pueden pasar parametros para realizar un una determinada accion.

Ejemplo practica de una funcion que llama a otra funcion para devolver un resultado

~~~python
def cuadrado() :
    numero = int(input("Ingresa un valor: "))
    numero = calculo(numero)
    return numero

def calculo(numero) :
    return numero * numero

resultado = cuadrado()

print("Resultado: ", resultado)

Salida >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
Ingresa un valor: 2
Resultado:  4
~~~

---

## Argumentos



---

## Modulos

Dividir el codigo en multiples archivos que pueden ser llamados entre si

Archivo: `modulo.py`
~~~python
def suma(valor1, valor2) :
    suma = valor1 + valor2

    return suma

def resta(valor1, valor2) :
    resta = valor1 - valor2

    return resta
~~~

Archivo: `main.py`
~~~python
import modulo
valor1 = int(input("Introdusca un valor: "))
valor2 = int(input("Introdusca otro valor: "))

print(modulo.suma(valor1, valor2))

print(modulo.resta(valor1, valor2))
~~~

*Nota: Se puede listar los modulos soportados por python introduciendo el comando `module` en el REPEL de python*

---

## Paquetes

Los paquetes son estructuras de carpetas y subcarpetas en donde podemos almacenar nuestros **modulos** para darle una mejor estructura a nuestro proyecto de codigo.
Se necesita agregar un archivo `__init__.py` para declarar que una carpeta o subcarpeta es un paquete.

Ejemplo de la estructura de un paquete

~~~python
sonido/                         # Carpeta
      __init__.py
      formatos/
              __init__.py
              webread.py
              wavwrite.py
              auread.py
      efectos/                  # Subcarpeta
              __init__.py
              echo.py
              surround.py
              reverse.py
      filtros/                  # Subcarpeta
              __init__.py
              equalizer.py
              vocal.py
              karaoke.py
~~~

Para llamar un modulo dentro de nuestro paquete solo se tiene que mandar llamar

~~~python
import paquete.modulo

print(paquete.modulo.funcion)
~~~

Otra forma de llamar a las funciones que estan en modulos dentro de paquetes sin tener que nombrar el paquete es la siguiente:

~~~python
from paquete import modulo

print(modulo.funcion)
~~~

---

## POO

*NOTA: Las funciones que se encuentran dentro de una clase son los llamados* ***metodos***.



---

# Para la Amiau

~~~python
archivo = open('datos.txt','w')

archivo.write('Esto es una linea de texto\n')

archivo.close()
~~~

~~~python
import pygame.mixer
from time import sleep
pygame.mixer.init()
pygame.mixer.music.load(open("ci.mp3"))
pygame.mixer.music.play()
while pygame.mixer.music.get_busy():
     sleep(1)
~~~

---

> [Git de Referencia](https://gitlab.com/Deckon/Python3)
